#!/bin/bash
#Title : NoTrack Blocklist Updater
#Description : This script will download latest Tracker block files from quidsup.net then upload to GitLab
#Author : QuidsUp
#Date : 2018-06-27
#Version : 2.1

readonly ANNOYANCE_DL_PLAIN="https://quidsup.net/notrack/blocklist.php?download=annoyance"
readonly ANNOYANCE_NEW_PLAIN="./notrack-annoyance.txt"
readonly ANNOYANCE_TEMP_PLAIN="/tmp/notrack-annoyance-daily.txt"

readonly ANNOYANCE_DL_HOSTS="https://quidsup.net/notrack/blocklist.php?download=annoyanceunix"
readonly ANNOYANCE_NEW_HOSTS="./annoyance.hosts"
readonly ANNOYANCE_TEMP_HOSTS="/tmp/notrack-annoyance-daily.hosts"

readonly ANNOYANCE_DL_LIST="https://quidsup.net/notrack/blocklist.php?download=annoyancedomains"
readonly ANNOYANCE_NEW_LIST="./annoyance.list"
readonly ANNOYANCE_TEMP_LIST="/tmp/notrack-annoyance-daily.list"

curdate=$(date +%Y-%m-%d)
updated=false

#######################################
# Download File
#   1. Download file with wget
#   2. Check return value of wget
#   3. Check if file exists
#
# Globals:
#   None
# Arguments:
#   1: Output File
#   2: URL
# Returns:
#   0 on success
#   > 0 on fail
#######################################
function download_file() {
  echo "Downloading $2"
  wget -qO "$1" "$2"                             #Download with wget
  
  exitstatus="$?"
  
  if [ $exitstatus -eq 0 ]; then
    if [ -s "$1" ]; then                         #Check if file has been downloaded
      return 0                                   #Success
    fi
  fi

  case $exitstatus in                            #Review exit code of wget
    "1") echo "Error: download_file - Generic error" ;;
    "2") echo "Error: download_file - Parsing error" ;;
    "3") echo "Error: download_file - File I/O error" ;;
    "4") echo "Error: download_file - Network error" ;;
    "5") echo "Error: download_file - SSL verification failure" ;;
    "6") echo "Error: download_file - Authentication failure" ;;
    "7") echo "Error: download_file - Protocol error" ;;
    "8") echo "Error: download_file - File not available on server" ;;
  esac

  exit 2
}


#######################################
# Check Size
#   Check the number of lines in a file is above expected
#
# Globals:
#   None
# Arguments:
#   1: File to check
#   2: Number of lines
# Returns:
#   None
#######################################
function check_size() {
  local actuallines=0
  local filename="$1"
  local expectedlines="$2"

  actuallines=$(wc -l "$filename" | cut -f1 -d\  )

  if [ "$actuallines" -lt "$expectedlines" ]; then
    echo "$filename below $expectedlines lines"
    exit 3
  fi
}


#######################################
# Compare Files
#   Compare files using diff excluding comment section
#   Copy if file has changed
# Globals:
#   None
# Arguments:
#   1: New File
#   2: Old File
# Returns:
#   0: File has changed
#   1: File is the same
#######################################
function compare_files() {
  local newfile="$1"
  local oldfile="$2"
  local diffoutput=0

  #Compare files quietly excluding comment lines
  diff -q -I "^#" "$1" "$2"
  diffoutput="$?"

  if [ $diffoutput -eq 0 ]; then
    echo "New file $newfile is the same as $oldfile"
    return 1
  else
    echo "Copying $newfile to $oldfile"
    cp "$newfile" "$oldfile"
    return 0
  fi
}


#######################################
# Main
#######################################

#NoTrack Formatted Lists
download_file "$ANNOYANCE_TEMP_PLAIN" "$ANNOYANCE_DL_PLAIN"
check_size "$ANNOYANCE_TEMP_PLAIN" 300
if compare_files "$ANNOYANCE_TEMP_PLAIN" "$ANNOYANCE_NEW_PLAIN"; then
  updated=true
  git add "$ANNOYANCE_NEW_PLAIN"
fi
echo

#Linux/Windows Hosts Formatted Lists
download_file "$ANNOYANCE_TEMP_HOSTS" "$ANNOYANCE_DL_HOSTS"
check_size "$ANNOYANCE_TEMP_HOSTS" 150
if compare_files "$ANNOYANCE_TEMP_HOSTS" "$ANNOYANCE_NEW_HOSTS"; then
  updated=true
  git add "$ANNOYANCE_NEW_HOSTS"
fi
echo

#DNS Formatted Lists
download_file "$ANNOYANCE_TEMP_LIST" "$ANNOYANCE_DL_LIST"
check_size "$ANNOYANCE_TEMP_LIST" 150
if compare_files "$ANNOYANCE_TEMP_LIST" "$ANNOYANCE_NEW_LIST"; then
  updated=true
  git add "$ANNOYANCE_NEW_LIST"
fi
echo

#Check if anything has been updated
if [ $updated == true ]; then
  echo "One or more files updated, committing code to GitLab"
  git commit -m "updated for $curdate"
  git push
else
  echo "No changes made, nothing to commit"
fi

