#!/bin/bash
#Title : NoTrack Annoyance Blocklist Creator
#Description : This script will create an ordered blocklist from master.csv
#Author : QuidsUp
#Date : 2020-11-24

#######################################
# Constants
#######################################
readonly ANNOYANCE_LIST="notrack-annoyance.txt"

#######################################
# Global Variables
#######################################
declare -a domainlist


#######################################
# Empty List File
#
# Globals:
#   ANNOYANCE_LIST
# Arguments:
#   None
# Returns:
#   None
#
#######################################
function empty_lists() {
  if [ ! -f $ANNOYANCE_LIST ]; then                        #Does file exist?
    echo "Creating $ANNOYANCE_LIST"
    touch $ANNOYANCE_LIST                                  #Create file
  fi

  cat /dev/null > $ANNOYANCE_LIST                          #Empty file

  #Write headers
  echo "#Title: NoTrack Annoyance Blocklist" | tee -a "$ANNOYANCE_LIST" &> /dev/null
  echo "#Description: Domains which fall outside of tracking and advertising, but are annoying if you inadvertently visit them" | tee -a "$ANNOYANCE_LIST" &> /dev/null
  echo "#Author: QuidsUp" | tee -a "$ANNOYANCE_LIST" &> /dev/null
  echo "#License: GNU General Public License v3.0" | tee -a "$ANNOYANCE_LIST" &> /dev/null
  echo "#Home: https://gitlab.com/quidsup/notrack-annoyance-blocklist" | tee -a "$ANNOYANCE_LIST" &> /dev/null
  echo "#@ GitLab : https://gitlab.com/quidsup/notrack-annoyance-blocklist/raw/master/notrack-annoyance.txt" | tee -a "$ANNOYANCE_LIST" &> /dev/null
  echo "#Updated: $(date +%Y-%m-%d)" | tee -a "$ANNOYANCE_LIST" &> /dev/null
  echo "" | tee -a "$ANNOYANCE_LIST" &> /dev/null
}


#######################################
# Git Commit
#
# Globals:
#   ANNOYANCE_LIST
# Arguments:
#   None
# Returns:
#   None
#
#######################################
function gitcommit() {
  git add annoyance-master.csv
  git add "$ANNOYANCE_LIST"
  git commit -m "updated for $(date +%Y-%m-%d)"
  git push
}

#######################################
# Process List
#   Read annoyance-master.csv and add domains into domainlist array
#
# Globals:
#   domainlist
# Arguments:
#   None
# Returns:
#   None
#
#######################################
function process_lists() {
  local master="annoyance-master.csv"

  while IFS=$',\n' read -r domain comment; do              #Read line of CSV
    domainlist+=("$domain #$comment - Annoyance")          #Add formatted line to array

  done < "$master"

  unset IFS
}


#######################################
# Sort List
#   Sort contents of domainlist and then save to file
#
# Globals:
#   ANNOYANCE_LIST, domainlist
# Arguments:
#   None
# Returns:
#   None
#
#######################################
function sort_lists() {
  local -a sortedlist                                      #Sorted array of domainlist

  echo "Sorting List"
  IFS=$'\n' sortedlist=($(sort -u <<< "${domainlist[*]}"))
  unset IFS

  echo "Number of Domains in block List: ${#sortedlist[@]}"
  echo "Writing block list to $ANNOYANCE_LIST"
  printf "%s\n" "${sortedlist[@]}" >> "$ANNOYANCE_LIST"
}

empty_lists
process_lists
sort_lists
gitcommit
